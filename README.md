# Laravel Test

1. Clone repo
2. Composer entry laravel/framework is missing the carrot symbol at the start of the version number
3. Install composer dependencies (command)
4. Run `php setup`
5. Install npm dependencies (command)
6. Compile assets
7. Add middleware `auth` to the welcome route
8. Create controller/job/migration/etc (command)
9. The view called `test` is broken because it has a bad filename.
10. Run unit tests
11. Correct query on /count route so it doesn't pull all records in the database
12. Convert query run on /cursor route to cursor lookup
13. How many models does the application have?
14. What relationships does the User model have?
15. Review contents of User->relationshipQuestion() and answer what it asks
16. The controller and view for the page shown at /welcome needs to be fixed. The page should show the users name and email address.
