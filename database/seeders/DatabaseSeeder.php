<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        tap(new User([
            'name' => 'test',
            'email' => 'test@example.com',
            'password' => Hash::make('test')
        ]))->save();

        User::factory()->count(99)->create();
    }
}
