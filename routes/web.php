<?php

use App\Http\Controllers\WelcomeController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Fake user credentials
// User: test@example.com
// Pass: test

Route::name('test')->group(function () {
    Route::get('/test', function () {
        return view('test');
    });
});


Route::get('/count', function () {
    return User::all()->count();
});


Route::get('/cursor', function () {
    $output = User::all()
        ->pluck(['name'])
        ->toArray();

    dump($output);
});


Route::get('/welcome', [WelcomeController::class, 'welcome'])
    ->name('welcome');


////////////////////////////////////////////////////////////////////////////////

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
